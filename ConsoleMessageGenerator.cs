﻿namespace UniversityLaboratory2._1;

public class ConsoleMessageGenerator
{
    public static void GenerateMessageFrom(string[] args, bool makeSpacing = false)
    {
        foreach (var s in args)
        {
            Console.WriteLine(s);
            if (makeSpacing)
            {
                Console.WriteLine();
            }
        }
    }

    public static void GenerateLaboratoryTitle(LaboratoryInfo laboratoryInfo)
    {
        GenerateMessageFrom(new string[]
        {
            "Лабораторная работа №" + laboratoryInfo.laboratoryId, 
            "",
            "Выполнил: "+laboratoryInfo.laboratoryOwner,
            "Группа: "+laboratoryInfo.laboratoryOwnerGroup,
            "Наименование ЛР: "+laboratoryInfo.laboratoryTitle,
        });
    }
}

public class LaboratoryInfo
{
    public string laboratoryOwner;
    public string laboratoryOwnerGroup;
    public int laboratoryId;
    public string laboratoryTitle;

    public LaboratoryInfo(string laboratoryOwner, string laboratoryOwnerGroup, int laboratoryId, string laboratoryTitle)
    {
        this.laboratoryOwner = laboratoryOwner;
        this.laboratoryOwnerGroup = laboratoryOwnerGroup;
        this.laboratoryId = laboratoryId;
        this.laboratoryTitle = laboratoryTitle;
    }
}