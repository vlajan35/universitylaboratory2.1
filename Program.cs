﻿namespace UniversityLaboratory2._1
{
    class Program
    {
        private static void Main()
        {
            ConsoleMessageGenerator.GenerateLaboratoryTitle(
                new LaboratoryInfo(
                    "Бродский Егор Станиславович",
                    "ИР-133Б",
                    1,
                    "СТРУКТУРА КОНСОЛЬНОГО ДОДАТКУ В C #"
                ));

            Console.WriteLine();
            Console.WriteLine($"D = {ExecuteFormula(-0.35)}");
            Console.WriteLine($"D = {ExecuteFormula(1.5)}");
        }

        private static double ExecuteFormula(double x)
        {
            return -Math.E * -Math.Cos(Math.Sqrt(x + 5.0 / 3)) -
                   1.7 * Math.Atan(x / 5.0 - 0.75) * Math.Sin(1.7 * x);
        }
    }
}