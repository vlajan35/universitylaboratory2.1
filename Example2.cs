﻿namespace UniversityLaboratory2._1;

public class Example2
{
    static void Example2Main()
    {
        Console.WriteLine(int.Parse(
                string.Join("",
                    (new string[] {"0"})
                    .Concat(
                        Console.ReadLine()?.ToCharArray()
                            .Select((c => c.ToString()))
                            .Where((s =>
                                    {
                                        try
                                        {
                                            int.Parse(s);
                                            return true;
                                        }
                                        catch (Exception e)
                                        {
                                            return false;
                                        }
                                    }
                                ))
                        ?? Array.Empty<string>()
                    )
                )
            )
        );
    }
}